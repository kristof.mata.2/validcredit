import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidatorTest {
    @Test
    public void tooShort() {
        Validator Challenge = new Validator();
        // Assert
        assertEquals(false, Challenge.validateCard(79927398714l));
    }

    @Test
    public void doesntPass() {
        Validator Challenge = new Validator();
        // Assert
        assertEquals(false, Challenge.validateCard(79927398713l));
    }

    @Test
    public void shouldPass() {
        Validator Challenge = new Validator();
        // Assert
        assertEquals(true, Challenge.validateCard(709092739800713l));
    }

    @Test
    public void shouldntPass() {
        Validator Challenge = new Validator();
        // Assert
        assertEquals(false, Challenge.validateCard(1234567890123456l));
    }

    @Test
    public void justLongEnoughShouldPass() {
        Validator Challenge = new Validator();
        // Assert
        assertEquals(true, Challenge.validateCard(12345678901237l));
    }

    @Test
    public void shouldPass2() {
        Validator Challenge = new Validator();
        // Assert
        assertEquals(true, Challenge.validateCard(5496683867445267l));
    }

    @Test
    public void shouldntPass2() {
        Validator Challenge = new Validator();
        // Assert
        assertEquals(false, Challenge.validateCard(4508793361140566l));
    }

    @Test
    public void shouldPass3() {
        Validator Challenge = new Validator();
        // Assert
        assertEquals(true, Challenge.validateCard(376785877526048l));
    }

    @Test
    public void shouldntPass3() {
        Validator Challenge = new Validator();
        // Assert
        assertEquals(false, Challenge.validateCard(36717601781975l));
    }

}