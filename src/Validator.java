import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;

public class Validator {

    public boolean validateCard(long cardNumber) {
        String cardString = Long.toString(cardNumber);
        String[] stringArray = cardString.split("");
        ArrayList<Integer> intArray = new ArrayList<>();
        int checkDigit;
        for(String s:stringArray){
            intArray.add(Integer.valueOf(s));
        }
        //Checking cardNumber length
        if(intArray.size()>19 || intArray.size()<14){ return false;}
        System.out.println(intArray.toString());
        checkDigit=intArray.get(intArray.size()-1);
        intArray.remove(intArray.size()-1);
        System.out.println(intArray.toString());
        Collections.reverse(intArray);
        System.out.println(intArray.toString());
        int sum=0;
        for(int i = 0;i<intArray.size();i++){
            if(i%2!=1){
                if((intArray.get(i)*2)>9){
                    String temp=Integer.toString(intArray.get(i)*2);
                    //System.out.printf("F: "+temp.substring(0,1)+" S: "+temp.substring(1));
                    intArray.set(i, Integer.valueOf(temp.substring(0,1))+Integer.valueOf(temp.substring(1)) );
                   // System.out.println("set: "+i+" to: "+Integer.valueOf(temp.substring(0,1))+"+"+Integer.valueOf(temp.substring(1)));
                }else{
                    intArray.set(i, intArray.get(i)*2);
                }

            }
            sum=sum+intArray.get(i);
        }

        System.out.println(intArray.toString());
        System.out.println("sum: "+sum);
        System.out.println("cd: "+checkDigit);
        System.out.println("ld: "+Integer.valueOf(Integer.toString(sum).substring(1)));
        if((10-Integer.valueOf(Integer.toString(sum).substring(1))) != checkDigit){
            return false;
        }
        return true;
    }
}
